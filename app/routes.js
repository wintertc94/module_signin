// app/routes.js
module.exports = function (app, passport) {
    // information processing when someone signup
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/profile', // move to page profile
        failureRedirect: '/signup', // comback page signup
        failureFlash: true // allow flash messages
    }));

    // information processing when someone login
    app.post('/login', passport.authenticate("local-login", {
        successRedirect : '/profile',
        failureRedirect : '/login',
        failureFlash : true
    }));

    // information processing when someone reset password
    app.post('/resetpassword', passport.authenticate("local-login", {
        successRedirect : '/profile',
        failureRedirect : '/resetpassword',
        failureFlash : true
    }));
    // =====================================
    // HOME PAGE ===========================
    // =====================================
    app.get('/', function (req, res) {
        res.render('index.ejs'); // load the index.ejs file
    });

    // =====================================
    // LOGIN ===============================
    // =====================================
    // Show form login
    app.get('/login', function (req, res) {
        // show page and send message from server (if any)
        res.render('login.ejs', {message: req.flash('loginMessage')});
    });
    // =====================================
    // SIGNUP ==============================
    // =====================================
    // Show form signup
    app.get('/signup', function (req, res) {
        res.render('signup.ejs', {message: req.flash('signupMessage')});
    });

    // =====================================
    // Change PASSWORD ======================
    // =====================================
    // Show form signup
    app.get('/changepassword', function (req, res) {
        res.render('changepassword.ejs', {message: req.flash('changepassword')});
    });

    // =====================================
    // Change PASSWORD ======================
    // =====================================
    // Show form signup
    app.get('/resetpassword', function (req, res) {
        res.render('resetpassword.ejs', {message: req.flash('resetpassword')});
    });

    // =====================================
    // PROFILE SECTION =====================
    // =====================================
    // show this page when you signed
    app.get('/profile', isLoggedIn, function (req, res) {
        res.render('profile.ejs', {
            user: req.user // Get info user in session and send to template
        });
    });

    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });
};

// route middleware to check a user signed or not?
function isLoggedIn(req, res, next) {
    // if user signed, go next
    if (req.isAuthenticated())
        return next();
    // if not, comeback to homepage
    res.redirect('/');
}